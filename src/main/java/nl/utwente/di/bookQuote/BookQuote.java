package nl.utwente.di.bookQuote;

import nl.utwente.di.ResourceHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/** Example of a Servlet that gets an ISBN and returns the book price
 */

public class BookQuote extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Quoter quoter;
	
    public void init() {
    	quoter = new Quoter();
    }	
	
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String template = new ResourceHandler().getFileContent("template.html");
    String title = "Book Quote";
    template = template.replaceAll("%t", title);
    String content = "<table>\n" +
            "<tr>\n" +
            "<td>ISBN:</td><td>" + request.getParameter("isbn") + "</td>\n" +
            "</tr><tr>\n" +
            "<td>Price:</td><td>" + quoter.getBookPrice(request.getParameter("isbn")) + "</td>\n" +
            "</tr>\n" +
            "</table>\n";
    out.println(template.replace("%c", content));
  }
}