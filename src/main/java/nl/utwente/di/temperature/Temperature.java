package nl.utwente.di.temperature;

import nl.utwente.di.ResourceHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;

public class Temperature  extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String template = new ResourceHandler().getFileContent("template.html");
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String temp = request.getParameter("temp");
        double k = Converter.toKelvin(temp, from);
        double newVal = Converter.fromKelvin(k, to);
        from = Converter.degSymbol(from);
        to = Converter.degSymbol(to);
        String title = String.format("Convert %s to %s", from, to);
        template = template.replaceAll("%t", title);
        String content = "<p>\n" +
                temp + " " + from + " = " + newVal + " " + to + "\n" +
                "</p>";
        template = template.replaceAll("%c", content);
        out.println(template);
    }
}