package nl.utwente.di.temperature;

public class Converter {
    public static double toKelvin(String value, String scale) {
        try {
            double d = Double.parseDouble(value);
            switch (scale) {
                case "K":
                    return d;
                case "C":
                    return d + 273.15;
                case "R":
                    return (d * 5) / 9.0;
                case "F":
                    return (((d - 32) * 5) / 9.0) + 273.15;
                default:
                    return 0;
            }
        } catch (NumberFormatException | NullPointerException exception) {
            return 0;
        }
    }
    public static double fromKelvin(double value, String scale) {
        try {
            switch (scale) {
                case "K":
                    return value;
                case "C":
                    return value - 273.15;
                case "R":
                    return value * 1.8;
                case "F":
                    return ((value - 273.15) * 1.8) + 32;
                default:
                    return 0;
            }
        } catch (NumberFormatException | NullPointerException exception) {
            return 0;
        }
    }

    public static String degSymbol(String scale) {
        switch (scale) {
            case "C":
            case "F":
                return "&deg;" + scale;
            default:
                return scale;
        }
    }
}
