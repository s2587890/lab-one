package nl.utwente.di;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class ResourceHandler {
    public String getFileContent(String fileName) {
        String content;
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            URL url = classLoader.getResource(fileName);
            if (url == null) {
                throw new NullPointerException();
            }
            File file = new File(url.getFile());
            InputStream stream = new FileInputStream(file);
            StringBuilder builder = new StringBuilder();
            try (Reader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
                int ch;
                while (-1 != (ch = reader.read())) {
                    builder.append((char) ch);
                }
            }
            content = builder.toString();
        } catch (NullPointerException | IOException ignored) {
            content = "<!DOCTYPE HTML>\n<html><head><title>Oops</title></head><body style='color:red'>Something went wrong.</body></html>";
        }
        return content;
    }
}
